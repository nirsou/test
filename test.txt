;;言語設定
(prefer-coding-system 'utf-8)

;; whitespace
(require 'whitespace)
(setq whitespace-style '(face           ; faceで可視化
			 trailing       ; 行末
			 tabs           ; タブ
			 spaces         ; スペース
			 empty          ; 先頭/末尾の空行
			 space-mark     ; 表示のマッピング
			 tab-mark
			 ))

(setq whitespace-display-mappings
      '((space-mark ?\u3000 [?\u25a1])
	;; WARNING: the mapping below has a problem.
	;; When a TAB occupies exactly one column, it will display the
	;; character ?\xBB at that column followed by a TAB which goes to
	;; the next TAB column.
	;; If this is a problem for you, please, comment the line below.
	(tab-mark ?\t [?\u00BB ?\t] [?\\ ?\t])))

;; スペースは全角のみを可視化
(setq whitespace-space-regexp "\\(\u3000+\\)")

(global-whitespace-mode 1)

(defvar my/bg-color "#232323")
(set-face-attribute 'whitespace-trailing nil
		    :background my/bg-color
		    :foreground "DeepPink"
		    :underline t)
(set-face-attribute 'whitespace-tab nil
		    :background my/bg-color
		    :foreground "LightSkyBlue"
		    :underline t)
(set-face-attribute 'whitespace-space nil
		    :background my/bg-color
		    :foreground "GreenYellow"
		    :weight 'bold)
(set-face-attribute 'whitespace-empty nil
		    :background my/bg-color)

;; font設定
(add-to-list 'default-frame-alist '(font . "ricty-13.5"))
(custom-set-faces
 '(variable-pitch ((t (:family "Ricty"))))
 '(fixed-pitch ((t (:family "Ricty"))))
 )

;;色設定
 ;; 文字の色を設定します。
(add-to-list 'default-frame-alist '(foreground-color . "white"))
;; 背景色を設定します。
;;(add-to-list 'default-frame-alist '(background-color . "black"))
;; カーソルの色を設定します。
(add-to-list 'default-frame-alist '(cursor-color . "SlateBlue2"))
;; マウスポインタの色を設定します。
(add-to-list 'default-frame-alist '(mouse-color . "SlateBlue2"))
;; モードラインの文字の色を設定します。
(set-face-foreground 'mode-line "SlateGray")
;; モードラインの背景色を設定します。
(set-face-background 'mode-line "white")
;; 選択中のリージョンの色を設定します。
(set-face-background 'region "LightSteelBlue1")
;; モードライン（アクティブでないバッファ）の文字色を設定します。
(set-face-foreground 'mode-line-inactive "gray60")
;; モードライン（アクティブでないバッファ）の背景色を設定します。
(set-face-background 'mode-line-inactive "gray85")
(set-face-foreground 'font-lock-comment-face "yellow")


;;python設定
(add-hook 'python-mode-hook
	  '(lambda ()
	     (setq indent-tabs-mode nil)
	     (setq indent-level 4)
	     (setq python-indent 4)
	     (setq tab-width 4)))



;;hilight
;(global-hl-line-mode t)                   ;; 現在行をハイライト
(show-paren-mode t)                       ;; 対応する括弧をハイライト
(setq show-paren-style 'mixed)            ;; 括弧のハイライトの設定。
(transient-mark-mode t)                   ;; 選択範囲をハイライト
;;
;; volatile-highlights
;;
;(require 'volatile-highlights)
;(volatile-highlights-mode t)

;;auto-intsall
;(add-to-list 'load-path (expand-file-name "~/.emacs.d/auto-install/"))
;(require 'auto-install)
;(auto-install-update-emacswiki-package-name t)
;(auto-install-compatibility-setup)

